import _ from 'lodash'
import * as types from './actionTypes'
import validator from 'validator'

const initialState = {
  isValid: false,
  errors: null,
  hasUsers: true
}

function validateState (state) {
  let errors = {}
  if (!validator.isLength(state.name, 1)) {
    errors.name = 'required'
  }
  return Object.assign(state, { errors, isValid: _.isEmpty(errors) })
}

export default function (state = initialState, action) {
  let { type, payload } = action
  switch (type) {
    case types.INITIALIZE_GROUP_FORM:
      return validateState(Object.assign({}, initialState, payload))

    case types.CHANGE_GROUP_FORM:
      return validateState(Object.assign({}, state, payload))

    default:
      return state
  }
}
