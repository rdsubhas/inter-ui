import _ from 'lodash'
import React from 'react'
import reactMixin from 'react-mixin'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import cx from 'classnames'

export default class GroupForm extends React.Component {

  render () {
    let { form, onChange, onSave, onDelete } = this.props
    let { errors } = form

    return (
      <div className='groups__form'>
        <h2>{form.isNew ? 'Add Group' : 'Edit Group'}</h2>

        <div className='clearfix mb1'>
          <label>Group name</label>
          <input type='text' className='block col-6 field' defaultValue={form.name}
            onChange={_.debounce((e) => onChange({ name: e.target.value }), 300)} />
          <span className='red'>{errors && errors.name}</span>
        </div>

        <button type='button' className='btn btn-primary' disabled={!form.isValid} onClick={() => onSave(form)}>Save</button>
        <button type='button' className={cx('btn btn-primary bg-red', { hide: form.isNew })} disabled={form.hasUsers} onClick={() => onDelete(form.id)}>{form.hasUsers ? 'Group has users' : 'Delete'}</button>
      </div>
    )
  }

}

GroupForm.propTypes = {
  form: React.PropTypes.object.isRequired,
  errors: React.PropTypes.array,
  onChange: React.PropTypes.func.isRequired,
  onSave: React.PropTypes.func.isRequired,
  onDelete: React.PropTypes.func.isRequired
}

reactMixin.onClass(GroupForm, PureRenderMixin)
