import _ from 'lodash'
import { pushPath } from 'redux-simple-router'
import * as groupsApi from 'common/api/groups'
import * as usersApi from 'common/api/users'
import * as types from './actionTypes'

export function initializeGroupForm (group) {
  return { type: types.INITIALIZE_GROUP_FORM, payload: group }
}

export function loadGroupUsers (groupId) {
  return dispatch => {
    usersApi.listUsers()
      .then(users => _.some(users, user => user.group_ids && user.group_ids.indexOf(groupId) >= 0))
      .then(hasUsers => dispatch(changeGroupForm({ hasUsers })))
  }
}

export function newGroupForm () {
  return initializeGroupForm({ id: new Date().getTime(), isNew: true })
}

export function loadGroupForm (id) {
  return dispatch => {
    groupsApi.getGroup(id)
      .then(group => dispatch(initializeGroupForm(group)))
  }
}

export function changeGroupForm (changes) {
  return { type: types.CHANGE_GROUP_FORM, payload: changes }
}

export function saveGroupForm (form) {
  return dispatch => {
    let group = _.omit(form, ['valid', 'errors', 'isNew'])
    groupsApi.saveGroup(group)
      .then(() => dispatch(pushPath('/groups')))
  }
}

export function deleteGroup (id) {
  return dispatch => {
    groupsApi.deleteGroup(id)
      .then(() => dispatch(pushPath('/groups')))
  }
}
