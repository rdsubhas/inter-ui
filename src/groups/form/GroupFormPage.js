import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from './actions'

import GroupForm from './views/GroupForm'

export default class GroupFormPage extends React.Component {

  initializeForm () {
    let groupId = parseInt(this.props.params && this.props.params.id, 10)
    if (groupId) {
      this.props.actions.loadGroupForm(groupId)
      this.props.actions.loadGroupUsers(groupId)
    } else {
      this.props.actions.newGroupForm()
    }
  }

  componentDidMount () {
    this.initializeForm()
  }

  render () {
    let { form, actions } = this.props
    return (
      <div>
        <GroupForm key={form.id} form={form}
          onChange={actions.changeGroupForm}
          onSave={actions.saveGroupForm}
          onDelete={actions.deleteGroup} />
      </div>
    )
  }

}

GroupFormPage.propTypes = {
  params: React.PropTypes.object,
  form: React.PropTypes.object.isRequired,
  actions: React.PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired
}

export default connect(
  state => ({ form: state.groups.form }),
  dispatch => ({ dispatch, actions: bindActionCreators(actions, dispatch) })
)(GroupFormPage)
