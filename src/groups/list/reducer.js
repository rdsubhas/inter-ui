import * as types from './actionTypes'

const initialState = {
  groups: []
}

export default function (state = initialState, action) {
  let { type, payload } = action
  switch (type) {
    case types.RECEIVE_GROUPS:
      return Object.assign({}, state, { groups: payload })
    default:
      return state
  }
}
