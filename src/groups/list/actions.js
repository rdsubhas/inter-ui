import * as groupsApi from 'common/api/groups'
import * as types from './actionTypes'

export function receiveGroups (groups) {
  return { type: types.RECEIVE_GROUPS, payload: groups }
}

export function refreshGroups () {
  return dispatch => {
    groupsApi.listGroups()
      .catch(() => [])
      .then(groups => dispatch(receiveGroups(groups)))
  }
}
