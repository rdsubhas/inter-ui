import React from 'react'
import reactMixin from 'react-mixin'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import cx from 'classnames'

export default class Groups extends React.Component {

  render () {
    let { groups } = this.props
    return (
      <div className='groups__list'>
        <h2>Groups</h2>
        <p className={cx({ hide: groups.length })}>No groups yet</p>
        <ul className='groups__items'>
          {this.renderGroups(groups)}
        </ul>
        <a href='#/groups/new' className='btn btn-primary'>Add a Group</a>
      </div>
    )
  }

  renderGroups (groups) {
    return groups.map(group => (
      <li key={group.id} className='group__item'>
        <a href={`#/groups/${group.id}`}>{group.name}</a>
      </li>
    ))
  }

}

Groups.propTypes = {
  groups: React.PropTypes.array.isRequired
}

reactMixin.onClass(Groups, PureRenderMixin)
