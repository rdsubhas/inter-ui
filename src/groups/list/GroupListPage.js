import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from './actions'

import Groups from './views/Groups'

export default class GroupListPage extends React.Component {

  componentDidMount () {
    this.props.actions.refreshGroups()
  }

  render () {
    let { list } = this.props
    return (
      <div>
        <Groups groups={list.groups} />
      </div>
    )
  }
}

GroupListPage.propTypes = {
  list: React.PropTypes.object.isRequired,
  actions: React.PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired
}

export default connect(
  state => ({ list: state.groups.list }),
  dispatch => ({ dispatch, actions: bindActionCreators(actions, dispatch) })
)(GroupListPage)
