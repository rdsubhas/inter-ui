import React from 'react'

export default class GroupsLayout extends React.Component {
  render () {
    return (
      <div className='groups'>
        {this.props.children}
      </div>
    )
  }
}

GroupsLayout.propTypes = {
  children: React.PropTypes.node.isRequired
}
