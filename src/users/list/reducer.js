import * as types from './actionTypes'

const initialState = {
  users: []
}

export default function (state = initialState, action) {
  let { type, payload } = action
  switch (type) {
    case types.RECEIVE_USERS:
      return Object.assign({}, state, { users: payload })
    default:
      return state
  }
}
