import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from './actions'

import Users from './views/Users'

export default class UserListPage extends React.Component {

  componentDidMount () {
    this.props.actions.refreshUsers()
  }

  render () {
    let { list } = this.props
    return (
      <div>
        <Users users={list.users} />
      </div>
    )
  }
}

UserListPage.propTypes = {
  list: React.PropTypes.object.isRequired,
  actions: React.PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired
}

export default connect(
  state => ({ list: state.users.list }),
  dispatch => ({ dispatch, actions: bindActionCreators(actions, dispatch) })
)(UserListPage)
