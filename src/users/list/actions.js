import * as usersApi from 'common/api/users'
import * as types from './actionTypes'

export function receiveUsers (users) {
  return { type: types.RECEIVE_USERS, payload: users }
}

export function refreshUsers () {
  return dispatch => {
    usersApi.listUsers()
      .then(users => dispatch(receiveUsers(users)))
  }
}
