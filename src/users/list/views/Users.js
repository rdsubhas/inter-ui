import React from 'react'
import reactMixin from 'react-mixin'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import cx from 'classnames'

export default class Users extends React.Component {

  render () {
    let { users } = this.props
    return (
      <div className='users__list'>
        <h2>Users</h2>
        <p className={cx({ hide: users.length })}>No users yet</p>
        <ul className='users__items'>
          {this.renderUsers(users)}
        </ul>
        <a href='#/users/new' className='btn btn-primary'>Add a User</a>
      </div>
    )
  }

  renderUsers (users) {
    return users.map(user => (
      <li key={user.id} className='user__item'>
        <a href={`#/users/${user.id}`}>{user.name}</a>
      </li>
    ))
  }

}

Users.propTypes = {
  users: React.PropTypes.array.isRequired
}

reactMixin.onClass(Users, PureRenderMixin)
