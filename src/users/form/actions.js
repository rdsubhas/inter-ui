import _ from 'lodash'
import { pushPath } from 'redux-simple-router'
import * as usersApi from 'common/api/users'
import * as groupsApi from 'common/api/groups'
import * as types from './actionTypes'

export function initializeUserForm (user) {
  return { type: types.INITIALIZE_USER_FORM, payload: user }
}

export function initializeUserGroups (groups) {
  return { type: types.INITIALIZE_USER_GROUPS, payload: groups }
}

export function loadUserGroups () {
  return dispatch => {
    groupsApi.listGroups().then(groups => dispatch(initializeUserGroups(groups)))
  }
}

export function newUserForm () {
  return initializeUserForm({ id: new Date().getTime(), isNew: true })
}

export function loadUserForm (id) {
  return dispatch => {
    usersApi.getUser(id).then(user => dispatch(initializeUserForm(user)))
  }
}

export function changeUserForm (changes) {
  return { type: types.CHANGE_USER_FORM, payload: changes }
}

export function saveUserForm (form) {
  return dispatch => {
    let user = _.omit(form, ['valid', 'errors', 'isNew', 'groups'])
    usersApi.saveUser(user)
      .then(() => dispatch(pushPath('/users')))
  }
}

export function deleteUser (id) {
  return dispatch => {
    usersApi.deleteUser(id)
      .then(() => dispatch(pushPath('/users')))
  }
}
