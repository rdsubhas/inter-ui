import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from './actions'

import UserForm from './views/UserForm'

export default class UserFormPage extends React.Component {

  initializeForm () {
    if (this.props.params && this.props.params.id) {
      this.props.actions.loadUserForm(this.props.params.id)
    } else {
      this.props.actions.newUserForm()
    }
    this.props.actions.loadUserGroups()
  }

  componentDidMount () {
    this.initializeForm()
  }

  render () {
    let { form, actions } = this.props
    return (
      <div>
        <UserForm key={form.id} form={form}
          onChange={actions.changeUserForm}
          onSave={actions.saveUserForm}
          onDelete={actions.deleteUser} />
      </div>
    )
  }

}

UserFormPage.propTypes = {
  params: React.PropTypes.object,
  form: React.PropTypes.object.isRequired,
  actions: React.PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired
}

export default connect(
  state => ({ form: state.users.form }),
  dispatch => ({ dispatch, actions: bindActionCreators(actions, dispatch) })
)(UserFormPage)
