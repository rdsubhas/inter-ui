import _ from 'lodash'
import React from 'react'
import reactMixin from 'react-mixin'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import cx from 'classnames'

export default class UserForm extends React.Component {

  toggleGroup (group_id) {
    let { form: { group_ids }, onChange } = this.props
    if (group_ids.indexOf(group_id) >= 0) {
      onChange({ group_ids: _.without(group_ids, group_id) })
    } else {
      onChange({ group_ids: _.union(group_ids, [group_id]) })
    }
  }

  render () {
    let { form, onSave, onDelete } = this.props
    return (
      <div className='users__form'>
        <h2>{form.isNew ? 'Add User' : 'Edit User'}</h2>
        <div className='user__profile mb1'>{this.renderForm()}</div>
        <div className='user__groups mb1'>{this.renderGroups()}</div>

        <button
          disabled={!form.isValid} type='button' className='btn btn-primary'
          onClick={() => onSave(form)}>Save</button>
        <button
          type='button' className={cx('btn btn-primary bg-red', { hide: form.isNew })}
          onClick={() => onDelete(form.id)}>Delete</button>
      </div>
    )
  }

  renderForm () {
    let { form, onChange } = this.props
    let { errors } = form
    return (
      <div>
        <label>User name</label>
        <input type='text' className='block field col-6' defaultValue={form.name}
          onChange={_.debounce((e) => onChange({ name: e.target.value }), 300)} />
        <span className='red'>{errors && errors.name}</span>
      </div>
    )
  }

  renderGroups () {
    let { form } = this.props
    let { errors, groups } = form
    let groupDivs = groups.map(group => (
      <div key={group.id}>
        <label>
          <input type='checkbox'
            checked={form.group_ids.indexOf(group.id) >= 0}
            onChange={() => this.toggleGroup(group.id)} />
          <span>{group.name}</span>
        </label>
      </div>
    ))

    return (
      <div>
        {groupDivs}
        <span className='red'>{errors && errors.group_ids}</span>
      </div>
    )
  }

}

UserForm.propTypes = {
  form: React.PropTypes.object.isRequired,
  errors: React.PropTypes.array,
  onChange: React.PropTypes.func.isRequired,
  onSave: React.PropTypes.func.isRequired,
  onDelete: React.PropTypes.func.isRequired
}

reactMixin.onClass(UserForm, PureRenderMixin)
