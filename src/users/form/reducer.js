import _ from 'lodash'
import * as types from './actionTypes'
import validator from 'validator'

const initialState = {
  isValid: false,
  errors: null,
  groups: [],
  group_ids: []
}

function validateState (state) {
  let errors = {}
  if (!validator.isLength(state.name, 1)) {
    errors.name = 'required'
  }
  if (!state.group_ids.length) {
    errors.group_ids = 'must be part of at least one group'
  }
  return Object.assign(state, { errors, isValid: _.isEmpty(errors) })
}

export default function (state = initialState, action) {
  let { type, payload } = action
  switch (type) {
    case types.INITIALIZE_USER_FORM:
      return validateState(Object.assign({}, initialState, payload))

    case types.INITIALIZE_USER_GROUPS:
      return validateState(Object.assign({}, state, { groups: payload }))

    case types.CHANGE_USER_FORM:
      return validateState(Object.assign({}, state, payload))

    default:
      return state
  }
}
