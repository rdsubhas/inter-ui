import React from 'react'

export default class UsersLayout extends React.Component {
  render () {
    return (
      <div className='users'>
        {this.props.children}
      </div>
    )
  }
}

UsersLayout.propTypes = {
  children: React.PropTypes.node.isRequired
}
