import { combineReducers as splitReducers } from 'redux'
import formReducer from './form/reducer'
import listReducer from './list/reducer'

const usersReducer = splitReducers({
  form: formReducer,
  list: listReducer
})

export default usersReducer
