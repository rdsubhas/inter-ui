import _ from 'lodash'
import when from 'when'
import localStorage from './localStorage'

export function listGroups () {
  return localStorage.keys()
    .then(keys => _.filter(keys, key => key.indexOf('groups.') === 0))
    .then(groupKeys => groupKeys.map(groupKey => localStorage.getItem(groupKey)))
    .then(promises => when.all(promises))
}

export function getGroup (id) {
  return localStorage.getItem(`groups.${id}`)
}

export function saveGroup (group) {
  return localStorage.setItem(`groups.${group.id}`, group)
}

export function deleteGroup (id) {
  return localStorage.removeItem(`groups.${id}`)
}
