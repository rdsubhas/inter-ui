import _ from 'lodash'
import when from 'when'
import localStorage from './localStorage'

export function listUsers () {
  return localStorage.keys()
    .then(keys => _.filter(keys, key => key.indexOf('users.') === 0))
    .then(userKeys => userKeys.map(userKey => localStorage.getItem(userKey)))
    .then(promises => when.all(promises))
}

export function getUser (id) {
  return localStorage.getItem(`users.${id}`)
}

export function saveUser (user) {
  return localStorage.setItem(`users.${user.id}`, user)
}

export function deleteUser (id) {
  return localStorage.removeItem(`users.${id}`)
}
