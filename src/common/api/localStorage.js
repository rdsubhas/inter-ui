import localforage from 'localforage'

localforage.config({
  name: 'internations',
  version: 2
})

export default localforage
