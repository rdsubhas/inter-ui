import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import appReducer from './appReducer'

const loggerMiddleware = createLogger()

const createStoreWithMiddleware = applyMiddleware(
  thunkMiddleware,
  loggerMiddleware
)(createStore)

export default function (initialState) {
  const store = createStoreWithMiddleware(appReducer, initialState)

  if (module.hot) {
    // Allow hot reloading reducer
    module.hot.accept('./appReducer', () => {
      store.replaceReducer(require('./appReducer').default)
    })
  }

  return store
}
