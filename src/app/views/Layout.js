import React from 'react'
import { pushPath } from 'redux-simple-router'

// Shortcut: Don't clear localStorage here, raise an action instead
// Since this is for dev/debugging, I'm doing this here
import localStorage from 'common/api/localStorage'

export default class Layout extends React.Component {
  render () {
    return (
      <div className='app'>
        <div className='app__header'>
          <div className='container'>
            <a href='#/' className='btn right' onClick={() => localStorage.clear(() => pushPath('/'))}>Clear Data</a>

            <a href='#/' className='btn'>Home</a>
            <a href='#/users' className='btn'>Users</a>
            <a href='#/groups' className='btn'>Groups</a>
          </div>
        </div>
        <div className='app__body'>
          {this.props.children}
        </div>
      </div >
    )
  }
}

Layout.propTypes = {
  children: React.PropTypes.node
}
