import React from 'react'
import { Router, Route, IndexRoute } from 'react-router'
import { Provider } from 'react-redux'

import Layout from './views/Layout'
import HomePage from 'home/HomePage'
import * as users from 'users'
import * as groups from 'groups'

import './css/app.css'

export default class App extends React.Component {
  render () {
    return (
      <Provider store={this.props.store}>
        <div>
          <Router history={this.props.history}>
            <Route path='/' component={Layout}>
              <IndexRoute component={HomePage} />
              <Route path='groups' component={groups.GroupsLayout}>
                <IndexRoute component={groups.GroupListPage} />
                <Route path='new' component={groups.GroupFormPage} />
                <Route path=':id' component={groups.GroupFormPage} />
              </Route>
              <Route path='users' component={users.UsersLayout}>
                <IndexRoute component={users.UserListPage} />
                <Route path='new' component={users.UserFormPage} />
                <Route path=':id' component={users.UserFormPage} />
              </Route>
            </Route>
          </Router>
        </div>
      </Provider>
    )
  }
}

App.propTypes = {
  history: React.PropTypes.object.isRequired,
  store: React.PropTypes.object.isRequired
}
