import { routeReducer } from 'redux-simple-router'
import { combineReducers as splitReducers } from 'redux'
import usersReducer from 'users/usersReducer'
import groupsReducer from 'groups/groupsReducer'

/**
 * Redux provides `combineReducers` for splitting keys in a state across
 * other reducers. I believe that should have been named `splitReducers`.
 * Similarly, there is an addon called `reduceReducers` which should have
 * really been `chainReducers`. This nomenclature has confused many other
 * people in my team before. So I import combineReducers as splitReducers.
 * And have to write this long disclaimer to avoid *further* confusion :(
 */

const appReducer = splitReducers({
  users: usersReducer,
  groups: groupsReducer,
  routing: routeReducer
})

export default appReducer
