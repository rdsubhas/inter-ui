import 'babel-polyfill'

import React from 'react'
import { render } from 'react-dom'
import App from './app/App'

import configureStore from './app/store'
import createHashHistory from 'history/lib/createHashHistory'
import { syncReduxAndRouter } from 'redux-simple-router'

// Sync "routing" object in state with window.location so that
// reducers/actions can be plain JS without a window reference
const store = configureStore(window.__INITIAL_STATE__)
const history = createHashHistory({ queryKey: false })
syncReduxAndRouter(history, store)

render(
  <App store={store} history={history} />,
  document.getElementById('app')
)
