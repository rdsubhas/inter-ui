import React from 'react'
import { connect } from 'react-redux'

export default class HomePage extends React.Component {
  render () {
    return (
      <div className='home'>
        Welcome, World!
      </div>
    )
  }
}

export default connect()(HomePage)
