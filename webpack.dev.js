var path = require('path')
var webpack = require('webpack')

var HtmlWebpackPlugin = require('html-webpack-plugin')
var postcssAutoprefixer = require('autoprefixer')
var postcssPrecss = require('precss')
var postcssImport = require('postcss-import')
var postcssExtend = require('postcss-extend')

module.exports = {
  devtool: 'cheap-module-source-map',
  entry: [
    './src/index',
    'webpack-hot-middleware/client'
  ],
  resolve: {
    root: path.join(__dirname, 'src')
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'index.js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: 'src/index.html'
    }),
    new webpack.NoErrorsPlugin(),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'babel',
      exclude: /node_modules/
    }, {
      test: /\.css$/,
      loader: 'style-loader!css-loader!postcss-loader'
    }]
  },
  postcss: function () {
    return [
      postcssImport({ path: ['node_modules'] }),
      postcssAutoprefixer,
      postcssPrecss,
      postcssExtend
    ]
  }
}
