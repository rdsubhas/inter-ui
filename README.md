## Development

* Run `npm install` and `npm start`
* Needs Node 4.x / npm v3.x or above

## Control flow

```
Render the App with Initial Global State
-> User interacts with Components
-> Components emit an Action/Message/Event
-> Goes to Store/Dispatcher/MessageBus/EventBus
-> Spreads to Reducers/Listeners/Processors/Handlers
-> Which handles the action and returns New Global State
-> Re-Render the App with New Global State
-> ... cycle continues ...
```

## Project Structure

* Used redux, react, webpack, babel and basscss
* I like grouping by functional scope (i.e. everything related to users in one folder) rather than by tech task (i.e. all reducers dumped in one folder, all actions in one, etc). The former is very compact and we don't have to jump across folders when developing. The latter is prevalent in the community, but I hate it, it leads to dead code in many places because people are not sure whether stuff is being used or not outside.
* `src/<route>/`: Everything related to a specific route
  * Nested routes are again separated as folders
  * `actionTypes.js`: All action (aka message/event) types
  * `actions.js`: All action emitters
  * `reducers.js`: All reducers (aka processors/handlers) which handle an action and return a new state
  * `<Route>Page.js`: Smart component for this route
  * `views/*`: Dumb components for this route
  * Dumb components purely work on props
  * [Smart vs dumb components](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0)
* `src/app/`: Core app files
  * `store.js`: The message bus (aka store/dispatcher)
  * `appReducer.js`: Combination of all reducers across all routes
* `src/index.js`: Entry point where the app begins and everything is tied together
* `src/index.html`: HTML template, script tag will be auto-injected inside this

## PENDING

* Have to move out right now, have been on this for 5 hours I guess and I don't want to drag this further. Will push the tests separately when I get the chance.
* I thought of having a Home Page with a small dashboard showing number of users/groups. Deciding against it for now.

## Endpoints

Right now, project uses Local Storage for saving data, but still uses promises. The endpoints should be:

### Groups:

* `GET /groups`
   Returns `{ data: [ { id, name }, ... ] }`
* `POST /groups`
   post body `{ id, name }`
* `GET /groups/:group_id`
   Returns `{ id, name }`
* `GET /groups/:group_id/users`:
   Returns `{ data: [ <user1>, <user2>, ... ] }`.
   This is used for checking whether groups contain a user or not.
   The user objects in response will have `id` and any other fields requested with `?fields`
* `PUT/POST/PATCH /groups/:group_id`
   Returns HTTP 2xx (200 with entity or 204 without)
* `DELETE /groups/:group_id`
   Returns HTTP 2xx (200 with entity or 204 without)

### Users:

* `GET /users`
  Returns `{ data: [ { id, name }, ... ] }`
* `POST /users`
  post body `{ id, name }`
* `GET /users/:user_id`
  Returns `{ id, name, group_ids }`
* `PUT/POST/PATCH /users/:user_id`
  Returns HTTP 2xx (200 with entity or 204 without)
* `DELETE /users/:user_id`
  Returns HTTP 2xx (200 with entity or 204 without)

All requests should return `400 Bad Request` if validations fail on the server side.

## Conventions

* Used [standard](https://github.com/feross/standard) for linting
* File naming
  * `TitleCase.js` for React components
  * `camelCase.js` for other files
  * This seems to be prevalent across React projects in GitHub
* Actions
  * Each action will have `type`, `payload`, `meta` and `error`. See [Flux Standard Action](https://github.com/acdlite/flux-standard-action)
